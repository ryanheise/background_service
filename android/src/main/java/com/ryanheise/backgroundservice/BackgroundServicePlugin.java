package com.ryanheise.backgroundservice;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.app.FlutterApplication;
import android.app.Activity;
import android.content.Context;
import io.flutter.view.FlutterCallbackInformation;
import io.flutter.view.FlutterMain;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import io.flutter.view.FlutterRunArguments;
import io.flutter.view.FlutterNativeView;
import io.flutter.plugin.common.PluginRegistry.PluginRegistrantCallback;

/** BackgroundServicePlugin */
public class BackgroundServicePlugin implements MethodCallHandler {
	private static final String CHANNEL_BACKGROUND_SERVICE = "ryanheise.com/background_service";
	private static final String CHANNEL_BACKGROUND_SERVICE_BG = "ryanheise.com/background_service_bg";

	private static MethodChannel channel;
	private static MethodChannel backgroundChannel;
	private static Registrar registrar;
	private static PluginRegistrantCallback pluginRegistrantCallback;

	public static void setPluginRegistrantCallback(PluginRegistrantCallback pluginRegistrantCallback) {
		BackgroundServicePlugin.pluginRegistrantCallback = pluginRegistrantCallback;
	}

	public static void registerWith(Registrar registrar) {
		BackgroundServicePlugin.registrar = registrar;
		channel = new MethodChannel(registrar.messenger(), CHANNEL_BACKGROUND_SERVICE);
		channel.setMethodCallHandler(new BackgroundServicePlugin());
		backgroundChannel = new MethodChannel(registrar.messenger(), CHANNEL_BACKGROUND_SERVICE_BG);
		backgroundChannel.setMethodCallHandler(new BackgroundServicePlugin());
	}

	@Override
	public void onMethodCall(MethodCall call, Result result) {
		switch (call.method) {
		case "start":
			FlutterApplication application = (FlutterApplication)registrar.activeContext().getApplicationContext();
			Activity activity = application.getCurrentActivity();
			final long callbackHandle = (Long)call.arguments;
			final FlutterCallbackInformation cb = FlutterCallbackInformation.lookupCallbackInformation(callbackHandle);
			final String appBundlePath = FlutterMain.findAppBundlePath(application);
			if (cb == null || appBundlePath == null)
				result.error("error", "error", null);
			else {
				BackgroundService.start(activity, new BackgroundService.ServiceListener() {
					@Override
					public void doTask(Context context) {
						FlutterNativeView backgroundFlutterView = new FlutterNativeView(context, true);
						pluginRegistrantCallback.registerWith(backgroundFlutterView.getPluginRegistry());
						FlutterRunArguments args = new FlutterRunArguments();
						args.bundlePath = appBundlePath;
						args.entrypoint = cb.callbackName;
						args.libraryPath = cb.callbackLibraryPath;
						backgroundFlutterView.runFromBundle(args);

						// Simulate doing a long task
						System.out.println("Service sleeping for 10 seconds...");
						try {
							Thread.sleep(10000);
						} catch (Exception e) {
							e.printStackTrace();
						}
						System.out.println("Service finished sleeping.");
					}
				});
				result.success(null);
			}
			break;
		default:
			result.notImplemented();
		}
	}
}

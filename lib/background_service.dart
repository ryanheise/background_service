import 'dart:async';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class BackgroundService {
  static const MethodChannel _channel = const MethodChannel('ryanheise.com/background_service');

  static Future<void> start(Function callback) async {
    final ui.CallbackHandle handle = ui.PluginUtilities.getCallbackHandle(callback);
    var callbackHandle = handle.toRawHandle();
    await _channel.invokeMethod('start', callbackHandle);
  }

  static MethodChannel initIsolate() {
    const MethodChannel backgroundChannel = const MethodChannel('ryanheise.com/background_service_bg');
    WidgetsFlutterBinding.ensureInitialized();
    return backgroundChannel;
  }
}

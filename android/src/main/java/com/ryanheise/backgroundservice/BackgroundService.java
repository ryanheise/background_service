package com.ryanheise.backgroundservice;

import android.app.IntentService;
import android.content.Context;
import io.flutter.app.FlutterApplication;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.app.Notification;
import android.content.Intent;

public class BackgroundService extends IntentService {
	private static final int NOTIFICATION_ID = 1;

	private static ServiceListener listener;

	public static synchronized void start(Context context, ServiceListener listener) {
		BackgroundService.listener = listener;
		context.startService(new Intent(context, BackgroundService.class));
	}

	public static synchronized void stop(Context context) {
		context.stopService(new Intent(context, BackgroundService.class));
	}

	public BackgroundService() {
		super("BackgroundService");
	}

	@Override
	public void onHandleIntent(final Intent intent) {
		FlutterApplication application = (FlutterApplication)getApplicationContext();
		PendingIntent openActivityPendingIntent = TaskStackBuilder.create(BackgroundService.this)
			.addNextIntentWithParentStack(new Intent(this, application.getCurrentActivity().getClass()))
			.getPendingIntent(0, 0);

		Notification.Builder builder = new Notification.Builder(BackgroundService.this);
		Notification notification = builder.setContentIntent(openActivityPendingIntent)
			.setOngoing(true)
			//.setSmallIcon(R.mipmap.ic_launcher)
			.setTicker("Test")
			.setAutoCancel(true)
			.setContentTitle("Test")
			.setContentText("Test")
			.setVisibility(Notification.VISIBILITY_PUBLIC)
			.setPriority(Notification.PRIORITY_MAX)
			.build();
		startForeground(NOTIFICATION_ID, notification);

		listener.doTask(this);

		stopForeground(true);
	}

	public static interface ServiceListener {
		void doTask(Context context);
	}
}

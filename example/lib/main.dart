import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:background_service/background_service.dart';
import 'package:path_provider/path_provider.dart';

void main() => runApp(new MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        appBar: new AppBar(
          title: const Text('Plugin example app'),
        ),
        body: new Center(
          child: new RaisedButton(
            child: new Text('Start'),
            onPressed: () {
              BackgroundService.start(_backgroundIsolate);
            },
          ),
        ),
      ),
    );
  }
}

void _backgroundIsolate() {
  MethodChannel channel = BackgroundService.initIsolate();
  hang();
}

void hang() async {
  print('getTemporaryDirectory');
  Directory tempDir = await getTemporaryDirectory();
  print('tempDir = $tempDir');
}

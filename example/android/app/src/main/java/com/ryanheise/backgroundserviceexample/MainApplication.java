package com.ryanheise.backgroundserviceexample;

import io.flutter.app.FlutterApplication;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugins.GeneratedPluginRegistrant;
import com.ryanheise.backgroundservice.BackgroundServicePlugin;

public class MainApplication extends FlutterApplication implements PluginRegistry.PluginRegistrantCallback {
	@Override
	public void onCreate() {
		super.onCreate();
		BackgroundServicePlugin.setPluginRegistrantCallback(this);
	}

	@Override
	public void registerWith(PluginRegistry registry) {
		GeneratedPluginRegistrant.registerWith(registry);
	}
}
